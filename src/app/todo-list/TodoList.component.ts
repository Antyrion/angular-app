import { Component, OnInit } from '@angular/core';
import { TodolistService } from './todolist.service';
@Component({

  selector: 'todo-list',
  templateUrl: './TodoList.component.html',
  styleUrls: ['./TodoList.component.css']
})
export class TodoList implements OnInit {

  newTodo: string;
  todos = [];

  constructor(private todolistService: TodolistService) {

  }

  addTask(task) {
    if (task) {
      this.todos.push({ title: task, key: Date.now() });
    }

  }

  removeTask(key) {
    var filteredItems = this.todos.filter(function (item) {
      return (item.key !== key);
    });
    this.todos = filteredItems;
  }

  ngOnInit() {
  }

}
