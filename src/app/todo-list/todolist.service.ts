import { Injectable } from '@angular/core';
import { TodoList } from './TodoList.component';
import { AngularFireDatabaseModule, AngularFireList, AngularFireDatabase } from 'angularfire2/database';
@Injectable()
export class TodolistService {

  toDoList: AngularFireList<any>

  constructor(private firebasedb: AngularFireDatabase) {

  }

  getToDoList() {
    this.toDoList = this.firebasedb.list('title');
    return this.toDoList;
  }

  addTitle(title: string) {
    this.toDoList.push({
      title: title
    });
  }


}

