// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCsNSRPenbAkJURssXIewfc2tAK2aFJLsk",
    authDomain: "todolist-445b8.firebaseapp.com",
    databaseURL: "https://todolist-445b8.firebaseio.com",
    projectId: "todolist-445b8",
    storageBucket: "todolist-445b8.appspot.com",
    messagingSenderId: "479189542789"
  }
};

